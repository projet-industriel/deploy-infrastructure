# IP bastion 
output "os_bastion_ext_ip" {
    description = "IP externe de l'instance bastion "
    value = "${openstack_networking_floatingip_v2.os_vm_bastion_fip}"
}
