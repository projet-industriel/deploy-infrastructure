#
# ------------ NETWORK SETUP --------------
###############################################
# Declaration du reseau prive du projet os
resource "openstack_networking_network_v2" "os_network" {
  name           = var.os_network.name
  admin_state_up = "true"
}

# Attribution de bloc de sous-reseau au reseau
resource "openstack_networking_subnet_v2" "os_subnet_internal" {
  name       = var.os_network.internal
  network_id = "${openstack_networking_network_v2.os_network.id}"
  cidr       = var.os_cidrs.internal
  ip_version = 4
  dns_nameservers = var.os_subnet_dns
}
